const Total = ({parts}) => {
    
    return (
        <p>
            Number of exercises: {parts.reduce((total, part) =>  parseInt(total)  +  parseInt(part.exercises), 0)}
        </p>
    )
}

export default Total;
