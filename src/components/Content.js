import Part from "./Part"
const Content = ({parts})=> {
    return (
      <div>
          <ul>
            {parts.map(part => 
              <li key={part.id}>
                { <Part part={part}/>}
              </li>
            )}
          </ul>
      </div>
    )
}

export default Content
