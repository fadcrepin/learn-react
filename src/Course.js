import Header from './components/Header';
import Content from './components/Content';
import Total from './components/Total';

const Course = ({courses}) => {
    return (
        <ul>
            {courses.map( course => 
            <li key={course.id}>
                <div>
                    <Header course={course}/>
                    <Content  parts={course.parts}/>
                    <Total parts={course.parts} />
                </div>
            </li>
            )}
        </ul>
    )
}

export default Course;
